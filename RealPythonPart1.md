# Part 1: Introduction to Python

## 1.1 Fundamentals: Strings and Methods

Surround strings with quotes. Either single or double quotes, but be consistent.

```python
phrase = 'hello world'
my_string = "We're #1!"
string_number = "1234"
conversation = 'I said, "put it over by the llama."'
```

Strings can include any characters - letters, numbers and symbols.

3 single quotes (or 3 double quotes) for creating multi-line strings. You can preserve whitespace if you use triple quotes.

```python
long_string = '''this is a string
that spans multiple lines'''
long_string2 = """this is a new string
that spans multiple lines"""
```

### 1.1.1 Mess Around with Your Words

"length" function: tell you the length of all sorts of things, including strings

```python
my_string = "abc"
string_length = len(my_string)
print(string_length)

string1 = "abra"
string2 = "cadabra"
magic_string = string1 + string2
print(magic_string)
```

Use square brackets to access characters inside a string.

```python
flavor = "birthday cake"
print(flavor[3])
print(flavor[0:3])
print(flavor[:5])
print(flavor[5:])
print(flavor[:])
```

* parentheses: ()
* square brackets: []
* curly braces: {}

### 1.1.2 Use Objects and Methods

### 1.1.3 Assignment: pick apart your user's input
